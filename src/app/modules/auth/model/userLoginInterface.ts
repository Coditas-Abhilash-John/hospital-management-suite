export interface IUserLoginCredentials {
  email: string;
  passwrod: string;
}
export interface IRole {
  title: string;
  roleId: string;
  url: string;
}
