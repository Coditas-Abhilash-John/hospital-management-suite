import { Injectable } from '@angular/core';
import { CanLoad, Route, Router, UrlSegment, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';



@Injectable({
  providedIn: 'root'
})
export class GuardService implements CanLoad {
  constructor(private router: Router) { }
  canLoad(route: Route, segments: UrlSegment[]): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
    if(window.localStorage.getItem('roleId') === environment.roles.find(role=>role.url === route.path)?.roleId && window.localStorage.getItem("token")){
      return true
    }
    alert('You are not authorized to visit this page. You are redirected to login Page');
    this.router.navigate([""],{ queryParams: { retUrl: route.path} });
    return false;
  }
}
