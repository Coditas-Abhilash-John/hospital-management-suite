import { ICustomInputProperties } from "./customInputInterface";

export interface  IInputDialogInput{
  dialogTitle : string,
  buttonType :string,
  alertInputAttributes : ICustomInputProperties[]
}
